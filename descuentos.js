const precioOriginal = 120;
const descuento = 18;

//manual
// console.log({
//     precioOriginal,
//     descuento,
//     porcentajePrecioConDescuento,
//     precioConDescuento,
// });

//encapsulado
function calcularPrecioConDescuento(precio, descuento) {
    
    const porcentajePrecioConDescuento = 100 - descuento;
    const precioConDescuento = (precio * porcentajePrecioConDescuento) / 100;
    
    return precioConDescuento;

}

function onClickButtonPriceDiscount(params) {
    const inputPrecio = document.getElementById("inputPrecio");
    const inputPreciovalue = inputPrecio.value;

    const inputDisc = document.getElementById("inputDisc");
    const inputDiscvalue = inputDisc.value;

    const precioConDescuenTototal = calcularPrecioConDescuento(inputPreciovalue, inputDiscvalue);

    return document.getElementById("totalprecio").innerText = "$" + precioConDescuenTototal;
}